Check24 Coding Challenge
=========================

This project contains a very small and feature limited blog software.



Requirements
------------

* PHP >= 7.0
* composer >= 1.5
* npm >= 5.6
* gettext >= 0.19

### For development

* sqlite3 >= 3.19
* phpunit >= 6.5

    (ideally, just the one from composer should be used, but when running it from vendor/bin/phpunit,
    it did not find any files, and I did not want to spend too much time in trying to find the problem,
     so I am executing the global phpunit for now)



Installation
------------

1. Run the following command:

    npm install

    If composer is not available as command "composer", also run ``composer install``

2. Create the database file:

    php vendor/bin/doctrine.php orm:schema-tool:create

3. Migrate the database:

    php vendor/bin/doctrine.php orm:schema-tool:update --force


Running the dev server
----------------------

Run the following command:

    npm start

The server will be available via



Running tests
-------------

Run the following command:

    npm test



Building JS and CSS sources
---------------------------

To build the sources, run this command:

   npm run build



Comments
--------
I did not have the time to actually make the site nicer. There is also only some Unittests..
I was mainly focussing on getting the project started with a little bit of MVC, As I would have hated code that is
just in an index page, etc.
