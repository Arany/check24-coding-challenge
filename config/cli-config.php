<?php

// replace with file to your own project bootstrap
require_once __DIR__ . '/../src/bootstrap.php';

use CCC\Service\Container;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

return ConsoleRunner::createHelperSet(Container::get('em'));