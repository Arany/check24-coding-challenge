<?php

/**
 * @TODO: add possibility for other environments
 */

$devMode = true;

return [
    "isDevMode" => $devMode,
    "db"        => [
        "driver" => "pdo_sqlite",
        "path"   => ROOT_DIR . '/database.sqlite',
    ],
    "twig" => [
        "cache" => ROOT_DIR . '/cache/templates',
        "debug" => $devMode,
    ],
    "routes" => require __DIR__ . '/routes.php',
];