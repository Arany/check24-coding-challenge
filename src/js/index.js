"use strict";

((document, window) => {
    const infoDiv = document.getElementById('info');
    const form = document.getElementById('createNew');

    form.onsubmit = event => {
        event.preventDefault();

        const request = new XMLHttpRequest();
        request.open('POST', '/api/entry');

        request.onload = () => {
            if (request.status === 200) {
                infoDiv.classList.remove('success');
                infoDiv.classList.add('error');
                infoDiv.innerText = "Success";  // @TODO: add translation

                // @TODO: make this actually ajax...
                window.location.reload();
            } else {
                infoDiv.classList.remove('success');
                infoDiv.classList.add('error');
                infoDiv.innerText = request.responseText;
            }
        };

        request.send(new FormData(form));
    };


})(document, window);
//# sourceMappingURL=index.js.map