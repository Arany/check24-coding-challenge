<?php

namespace CCC;


use CCC\Controller\ControllerInterface;
use CCC\Service\Container;
use Exception;

class App
{
    /**
     * @var string[]
     */
    protected $routes;

    /**
     * @param string[] $routes An array of: [<route regex> => <controller>]
     */
    public function __construct(array $routes)
    {
        $this->routes = $routes;
    }

    /**
     * Searches for the first route that matches the query pattern
     * and executes it's action for the corresponding HTTP method.
     * @return bool If the dispatch was used on a valid path (and not on a static file)
     */
    public function dispatch(): bool
    {
        // @TODO: add nicer error messages

        $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $method = strtolower($_SERVER['REQUEST_METHOD']);

        if ($this->isStaticFilePath($path)) {
            return false;
        }


        $controller = $this->getController($path);

        if (!$controller) {
            return $this->respond("Page not found", 404);
        }

        $functionName = "${method}Action";

        // check if the HTTP method is actually allowed
        if (!method_exists($controller, $functionName)) {
            return $this->respond("Method not allowed", 405);
        }

        try {
            // execute the controller action
            return $this->respond($controller->$functionName());
        } catch (Exception $e) {
            http_response_code(500);
            // @TODO: add error log, currently, this shows the error in the unittests
            //error_log($e->getMessage() . PHP_EOL . $e->getTraceAsString());

            if (Container::get('config')['isDevMode']) {
                $message = $e->getMessage() . PHP_EOL . $e->getTraceAsString();
            } else {
                $message = "Internal Server Error";
            }
            return $this->respond($message);
        }
    }

    /**
     * @TODO use a response class, so that you can set response headers from controllers
     * @param string $text
     * @param int    $statusCode
     * @return true
     */
    private function respond(string $text, int $statusCode = 200): bool {
        http_response_code($statusCode);
        echo $text;

        return true;
    }

    /**
     * Returns the controller for the given path
     * @param string $path
     * @return ControllerInterface|null
     */
    private function getController(string $path)
    {
        foreach ($this->routes as $regex => $controller) {
            if (preg_match($regex, $path, $matches)) {
                if (is_string($controller)) {
                    return new $controller(Container::get('twig'));
                } else {
                    return $controller;
                }
            }
        }

        return null;
    }

    /**
     * If
     * @param string $path
     * @return false|int
     */
    private function isStaticFilePath(string $path)
    {
        return preg_match("#^/static/#", $path);
    }
}