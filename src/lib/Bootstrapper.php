<?php


namespace CCC;


use CCC\Service\Container;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Extensions\I18nExtension;
use Twig\Loader\FilesystemLoader;

class Bootstrapper
{
    /**
     * Bootstraps the application and adds global services.
     * I am using a function here, because it might be easier to extend and test.
     * Also, I can create variables that are not available in global name space,
     * unlike if I would do that in the boostrap.php
     */
    public static function bootstrap()
    {
        $config = require ROOT_DIR . '/config/config.php';

        // adding global configuration
        Container::add('config', $config);


        /*******************************
         *
         * adding global services
         *
         *******************************/

        // adding doctrine
        $em = EntityManager::create(
            $config['db'],
            Setup::createAnnotationMetadataConfiguration([
                ROOT_DIR . '/src/lib/Entity',
            ], $config['isDevMode'])
        );
        Container::add('em', $em);

        // adding twig
        $twig = new Environment(new FilesystemLoader([
            ROOT_DIR . '/src/templates',
            ROOT_DIR . '/cache/static'
        ]), $config['twig']);
        $twig->addExtension(new I18nExtension());
        $twig->addExtension(new DebugExtension());
        Container::add('twig', $twig);
    }
}