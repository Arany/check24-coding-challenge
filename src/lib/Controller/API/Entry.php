<?php


namespace CCC\Controller\API;


use CCC\Controller\BaseController;
use CCC\Entity\Entry as EntryEntity;
use CCC\Service\Container;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;

class Entry extends BaseController
{
    /**
     * Creates a new blog entry.
     * @return string
     */
    public function postAction(): string
    {
        // I am currently just putting stuff in the db and sanitizing when getting it out
        $title = $_POST['title'];
        $text = $_POST['text'];

        if (empty($title) || empty($text)) {
            return json_encode([
                "error" => true,
            ]);
        }

        $entry = new EntryEntity();
        $entry->title = $title;
        $entry->text = $text;

        /** @var EntityManager $em */
        $em = Container::get('em');
        $em->persist($entry);
        try {
            $em->flush();
        } catch (OptimisticLockException $e) {
            return json_encode([
                "error"   => true,
                "message" => $e->getMessage(),
            ]);
        }


        return json_encode($entry);
    }
}