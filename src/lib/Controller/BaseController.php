<?php


namespace CCC\Controller;


use Twig\Environment;

abstract class BaseController implements ControllerInterface
{
    /**
     * @var Environment
     */
    protected $twig;

    /**
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }
}