<?php


namespace CCC\Controller;


use Twig\Environment;

interface ControllerInterface
{
    /**
     * ControllerInterface constructor.
     * @param Environment $twig
     */
    public function __construct(Environment $twig);
}