<?php


namespace CCC\Controller;


use CCC\Entity\Entry;
use CCC\Service\Container;
use Doctrine\ORM\EntityManager;

class Index extends BaseController
{
    /**
     * The main view of the blog.
     *
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function getAction(): string
    {
        /** @var EntityManager $em */
        $em = Container::get('em');
        $repository = $em->getRepository(Entry::class);

        // @TODO: add pagination
        $blogEntries = $repository->findBy([], ["id" => "DESC"], 20);
        return $this->twig->render('pages/index.twig', [
            "blogEntries" => $blogEntries,
        ]);
    }
}