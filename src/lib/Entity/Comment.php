<?php


namespace CCC\Entity;

use DateTime;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * Class Comment
 * @Entity
 */
class Comment
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="CCC\Entity\Entry", inversedBy="comments")
     * @var Entry
     */
    private $entry;

    /**
     * @Column(type="datetime")
     * @var DateTime
     */
    private $createdAt;

    /**
     * @Column(type="text")
     * @var string
     */
    private $authorName;

    /**
     * @Column(type="text")
     * @var string
     */
    private $text;

    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function __get(string $name)
    {
        return $this->$name;
    }
}