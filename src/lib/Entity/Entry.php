<?php


namespace CCC\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OrderBy;
use Exception;
use JsonSerializable;


/**
 * Class Entry
 * @Entity
 */
class Entry implements JsonSerializable
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @Column(type="datetime")
     * @var DateTime
     */
    private $createdAt;

    /**
     * @Column(type="text")
     * @var string
     */
    private $title;

    /**
     * @Column(type="text")
     * @var string
     */
    private $text;

    /**
     * @OneToMany(targetEntity="CCC\Entity\Comment", mappedBy="entry", orphanRemoval=true)
     * @OrderBy({"id": "ASC"})
     * @var ArrayCollection
     */
    private $comments;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->createdAt = new DateTime();
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function __get(string $name)
    {
        return $this->$name;
    }

    /**
     * @param string $name
     * @param mixed  $value
     * @return mixed  The input vale
     * @throws Exception
     */
    public function __set(string $name, $value)
    {
        if (in_array($name, [
            "title", "text",
        ])) {
            return $this->$name = $value;
        }
        throw new Exception("You cannot change this value.");
    }

    /**
     * Returns a text in a parsed form
     * @return string
     */
    public function parseText(string $text)
    {
        // @TODO: add a text parser, like markdown or bbcode
        // Twig is already sanitizing text by default, so we do not have to do it here.
        return $text;
    }

    /**
     * Returns the title in a parsed form
     * @return string
     */
    public function parsedTitle(): string
    {
        return $this->parseText($this->title);
    }

    /**
     * Returns the text in a parsed form
     * @return string
     */
    public function parsedText(): string
    {
        return $this->parseText($this->text);
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id'        => $this->id,
            'title'     => $this->title,
            'text'      => $this->text,
            'createdAd' => $this->createdAt,
        ];
    }
}