<?php
/**
 * A container that can hold any object to be available globally.
 */

namespace CCC\Service;

use InvalidArgumentException;

class Container
{
    /**
     * All services that are added
     * @var array
     */
    protected static $services = [];

    /**
     * @param string $name    The name the service should be retrieved with
     * @param mixed  $service The service to be added to the container
     * @throws InvalidArgumentException
     * @return null
     */
    public static function add(string $name, $service)
    {
        if (empty($name)) {
            throw new InvalidArgumentException("The name of the service must not be empty.");
        }

        self::$services[$name] = $service;

        return null;
    }

    /**
     * @param string $name The name of the service to retrieve
     * @return mixed The service with that name
     */
    public static function get(string $name)
    {
        if (!isset(self::$services[$name])) {
            throw new InvalidArgumentException(`No service with name $name exists.`);
        }

        return self::$services[$name];
    }
}