<?php


namespace CCC\Test;

use CCC\Bootstrapper;
use CCC\Service\Container;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Twig\Environment;

class BootstrapperTest extends TestCase
{
    /**
     * @covers Bootstrapper::bootstrap
     */
    public function testBootstrapInitializesServiceContainer()
    {
        /*
         * since I am currently bootstrapping when loading phpunit,
         * I can directly test for the containers, without actually executing the bootstrap method.
         *
         * Ideally, We would not really test the contents of the Container, just that it does something,
         * otherwise you test for the definition that you anyways put into the bootstrap method.
         */

        $this->assertArrayHasKey('isDevMode', Container::get('config'));

        $this->assertInstanceOf(EntityManager::class, Container::get('em'));
        $this->assertInstanceOf(Environment::class, Container::get('twig'));
    }
}