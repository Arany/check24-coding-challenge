<?php

namespace CCC\Test\Service;

use CCC\Service\Container;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class ContainerTest extends TestCase
{

    /**
     * @expectedException InvalidArgumentException
     */
    public function testAddThrowsExceptionIfTheNameIsEmpty()
    {
        Container::add('', []);
    }

    public function testAddsAService() {
        $service = new \stdClass();

        $this->assertNull(Container::add('service.name', $service));

        return $service;
    }

    /**
     * @param mixed $service
     * @depends testAddsAService
     */
    public function testGetRetrievesTheService($service) {
        $this->assertSame($service, Container::get('service.name'));
    }
}
