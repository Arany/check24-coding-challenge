<?php


namespace CCC\Test;

use CCC\Service\Container;
use Exception;
use CCC\App;
use PHPUnit\Framework\TestCase;

class AppTest extends TestCase
{

    public function dispatchDataProvider(): array
    {
        return [
            ["GET", '/test', 200, "yay"],
            ["GET", '/test/', 200, "yay"],
            ["GET", '/test/123', 200, "yay"],
            ["GET", '/test/123/', 200, "yay"],
            ["POST", '/test', 405, "Method not allowed"],
            ["GET", '/foo', 404, "Page not found"],
            ["POST", '/foo', 404, "Page not found"],
        ];
    }

    /**
     * @dataProvider dispatchDataProvider
     */
    public function testDispatch($method, $requestUrl, $expectedStatusCode, $expectedOutput)
    {
        $mock = $this->getMockBuilder('stdClass')
            ->setMethods(["getAction"])
            ->getMock();

        $mock->expects($expectedStatusCode == 200 ? $this->once() : $this->never())
            ->method('getAction')
            ->willReturn('yay');

        $app = new App([
            "#^/test(?:/(\d.+))?/?$#" => $mock,
        ]);

        $_SERVER['REQUEST_METHOD'] = $method;
        $_SERVER['REQUEST_URI'] = $requestUrl;

        ob_start();
        $app->dispatch();
        $this->assertEquals($expectedOutput, ob_get_clean());

        // @TODO add status code test
    }

    public function testServerError()
    {
        $exception = new Exception("an error");

        $mock = $this->getMockBuilder('stdClass')
            ->setMethods(["getAction"])
            ->getMock();

        $mock->expects($this->exactly(2))
            ->method('getAction')
            ->willThrowException($exception);

        $app = new App([
            "#^/test/$#" => $mock,
        ]);

        $_SERVER['REQUEST_METHOD'] = "GET";
        $_SERVER['REQUEST_URI'] = "/test/";

        $config =  Container::get('config');

        // in debug mode
        $config['isDevMode'] = true;
        Container::add('config', $config);
        ob_start();
        $app->dispatch();
        $this->assertEquals($exception->getMessage() . PHP_EOL . $exception->getTraceAsString(), ob_get_clean());

        // in production mode
        $config['isDevMode'] = false;
        Container::add('config', $config);
        ob_start();
        $app->dispatch();
        $this->assertEquals("Internal Server Error", ob_get_clean());
    }
}