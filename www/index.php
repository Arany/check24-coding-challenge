<?php

require_once __DIR__ . '/../src/bootstrap.php';

use CCC\App;
use CCC\Service\Container;

$app = new App(Container::get('config')['routes']);
return $app->dispatch();
